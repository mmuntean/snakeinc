// This is a project that aims to recreate in C the famous snake game.
// As good as I can recreate the game in C. 
//

#include <stdio.h>
#include <stdlib.h>
#include <termios.h> //For more info: pubs.openfroup.org/onlinepubs/7908799/xsh/termios.h.html
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <fcntl.h>

// de facut
//  - implementat ca mancarea sa fie generata si sa apara pe ecran
//  - implementat ca entitatea cand mananca mancarea sa se lungeasca  cu 1 B
//  - implementat ca ciocnirea entitatii de propriul corp sa termine jocul

//console: 36 l; 100 c

//Constants

#define UP_ARROW (char) 'w'
#define LEFT_ARROW (char) 'a'
#define RIGHT_ARROW (char) 'd'
#define DOWN_ARROW (char) 's'

#define ENTER_KEY 10

#define SNAKE_HEAD (char) 'H'
#define SNAKE_BODY (char) 'B'
#define FOOD (char) 'X'
#define BLANK (char) ' ' 

#define SNAKE_MAX_LENGTH (int) 30
#define SCREEN_HEIGHT (int) 36
#define SCREEN_WIDTH (int) 100
// Protoypes

void  clrscr();
int   collisionDetector(int hX[], int hY[], int x, int y, int cL);
void  directionalInput(int dir[]);
void  drawInit(int hX[], int hY[], int x, int y,int iL);
void  drawSnake(int hX[], int hY[],int cl);
char  getKeyWhenPressed();
int   kbHit();
void  moveCursor(int x, int y);
void  pickFoodLocation(int x, int y, int array[]);
void  printFoodItem(int* p);
void  shiftingArray(int array[], int cL, int mode);
void  startSnakeGame();
int   verifyBounds(int x, int y);
int   verifyFoodPick(int x, int y, int* locFood);
void  updateHeadPosition(int hX[] , int hY[], int *arrayP);

// Functions

int main()
{
	clrscr();
	startSnakeGame();
	clrscr();
	return 0;
}

/* in the order declared in prototype section, the functions:  */

// clrscr();
// This function erase the text from the console.
// Parameters: none
// Return: void
void clrscr()
{
	system("clear");
}

// collisionDetector(int hX[], int hY[], int x, int y)
// That function tests, all the entries in hX and hY from the end to the beggining of the arrays,
// if the coord values are equal to one of the paired values of the hX[] and hY[]. In the case
// that is equal, that means that a collision occured.
// Parameters: int hX[] - an array that stores the history of X coordinates
//	       int hY[] - an array that stores the history of Y coordinates
//	       int x    - an integer that stores the X coordinate of the head 
//	       int y    - an integer that stores the Y coordinate of the head
//	       int cL   - an integer that stores current length of the Snake
// Return: 1 if is a collision, else 0
int collisionDetector(int hX[], int hY[], int x, int y, int cL)
{
	int boolean=0; // we assume that is not a collsion
	int i;
	for (i=1;i>cL;i++)
		if ((hX[i]==x) && (hY[i]==y))
			boolean=1; //  it is a collision
	return boolean;
}

// directionalInput()
// Translates the user command in numerical friendly value.
// Parameters: void
// Return: int[2] -> int[0] stores the x coordinate
//		  -> int[1] stores the y coordinate
void directionalInput(int direction[])
{
	direction[0] = 0;
	direction[1] = 0;
	char c = getKeyWhenPressed();
	switch (c)
	{
		case UP_ARROW : { direction[0] = -1; direction[1] = 0;} break;
		case DOWN_ARROW : { direction[0] = 1; direction[1] = 0; } break;
		case RIGHT_ARROW : { direction[0] = 0; direction[1] = 1; } break;
		case LEFT_ARROW : { direction[0] = 0; direction[1] = -1; } break;
		default: { direction[0] = 0; direction[1] = 0; }
	}
}

// drawInit(int hX[], int hY[], int x, int y);
// This function prints on the canvas begining at the coordinates (x,y) a SNAKE_HEAD character and
// 3 SNAKE_BODY characters. Populates hX, hY with the coordinates of the drawing.
// Parameters: hX - array with SNAKE_MAX_LENGTH locations
//	       hY -  array with SNAKE_MAX_LENGTH locations
//             x - the x value of the pair of coordinates
//             y - the y value of the pair of coordinates
//	       initLength - the initial length of the snake
// Return: through parameters hX,hY with 4 locations occupied
void drawInit(int hX[], int hY[], int x, int y, int initLength)
{
	moveCursor(x, y);
	printf("%c", SNAKE_HEAD);
	hX[0]=x;
	hY[0]=y;
	x--;
	
	int i;
	for (i=1;i<initLength;i++)
	{
		hX[i]=x;
		hY[i]=y;

		moveCursor(hX[i], hY[i]);
		printf("%c", SNAKE_BODY);
		x--;
	}
	
	fflush(stdout);	
}


// drawSnake(int hX[],int hY[],int cL);
// This function draws the snake on the canvas.
// Parameters:  hX[] - an array with SNAKE_MAX_LENGTH locations
//		hY[] - an array with SNAKE_MAX_LENGTH locations
//		cL   - an integer that stores the length of the snake
// Return: void
void drawSnake(int hX[], int hY[], int cL)
{ // attention: possibility of remaining in place after automatization

	moveCursor(hX[0], hY[0]);
	printf("%c", SNAKE_HEAD);
	int i;
	for (i=1;i<cL;i++)
	{
		moveCursor(hX[i], hY[i]);
		printf("%c", SNAKE_BODY);
	}
	
	fflush(stdout);	
}


// getKeyWhenPressed()
// Test if a key on the keyboard was pressed.
// Parameters: none
// Return: char, the ascii code of the key pressed
char getKeyWhenPressed()
{
	char c=NULL;
	if (kbHit())
	{
		c=getchar();
	}
	return c;
}


// kbHit()
// Verifies if a key was pressed upon calling.
// Code from : https://cboard.cprogramming.com/c-programming/63166-kbhit-linux.html
// Paraneters: none
// Return:int 1 -> if was pressed a key on keyboard
//		0 -> if not
int kbHit()
{
	struct termios oldt;
	struct termios newt;
	int ch;
	int oldflag;
	int r;

	tcgetattr(STDIN_FILENO, &oldt); 
	/*
		int tcgetattr(int fildes, struct termios *termios_pointer)	
		Usage: Get the parameters associated with the terminal refferred to by fildes and store them in the termios structure refferenced by termios_pointer
		Parameters: fildes -> open file descriptor asociated with the terminal
			    termios_pointer -> a pointer to a termios struct
		OBS: STDIN_FILENO is a file descriptor and determines the default way of io communication - the console. It is a macro defined in /usr/include/unistd.h
		For more info: pubs.opengroup.org/oninepubs/007904975/functions/tcgetattr.html
	*/
	newt = oldt;
	newt.c_lflag &= ~(ICANON | ECHO);
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	/*
		int tcsetattr(int fildes, int optional_actions, const struct termios *termios_p)
		Usage: In this mode, set the parameters associated with the terminal refferred to by fildes and store them in the termios structure refferenced by termios_p. Because optional_actions is set to TCSANOW, the change shall occur immidiately.
		More info: pubs.opengroup.org/oninepubs/007904975/functions/tcsetattr.html
	*/

	oldflag = fcntl(STDIN_FILENO, F_GETFL, 0);
	fcntl(STDIN_FILENO, F_SETFL, oldflag | O_NONBLOCK);
	/*
		int fcntl(int filedes, int cmd, ... )
		Usage: Set a file status flags, defined in <fcntl.h>
		More info: pubs.opengroup.org/onlinepubs/009604599/functions/fcntl.html
		Example: can be used for locking and unlocking a file
	*/

	ch = getchar();
	/*
		int getchar(void)
		Usage: Reads a single character from the standard imput stream stdin, and returns itas a code.
		More info: cplusplus.com/reference/cstdio/getchar/

	*/

	tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
	fcntl(STDIN_FILENO, F_SETFL, oldflag);

	if (ch !=EOF)
	{
		ungetc(ch, stdin);
		/*
			int ungetc(int char, FILE *stream)
			Usage: inserts int char in the stream FILE *stream.
			More info: tutorialspoint.com/c_standard_library/c_function_ungetc.htm
		*/
		r=1;
	}
	else r=0;
	return r;
}


// moveCursor(int x, int y);
// Move the console cursor to a specific destination. Uses ESC character : 0x1b.
// To be used in a console with text writing mode enabled.
// Parameters: int x -> number of lines
//		int y -> number of collumns
// Return: void
void moveCursor(int x, int y)
{
	printf("%c[%d;%df",0x1B,x,y);
	fflush(stdout);
}


// pickFoodLocation(int x, int y);
//
// Parameters : int x -> number of lines of the canvas
//		int y -> number of collumns of the canvas
// Return: int*, a pointer to an array of 2 with the coordinates of the food item
void pickFoodLocation(int x, int y, int food[])
{
	food[0] = (rand() % (x-0+1))+0;
// For more info: https://geeksforgeeks.org/generating-random-number-range-c/
	food[1] = (rand() % (y-0+1))+0;
}


// printFoodItem(int lines, int collumns)
// Generates and prints the food item on a screen of lines lines and collumns collumns.
// Parameters:  int lines-> the number of the lines available on the screen 
//		int collumns-> the number of the collumns available on the screen
// Return: void
void printFoodItem(int* loc)
{
	//int* p=pickFoodLocation(lines, collumns);
	moveCursor(*loc,*(loc+1));
	printf("%c", FOOD);
	fflush(stdout);
}


// shiftingArray(int[] array,int cL,int mode)
// This function shifts the values of array[] with step one, 
// to the right from position 2 when mode==0 and from position 1 when mode==1.
// Parameters: array - an array with SNAKE_MAX_LENGTH elements
//		cL - the current length of the snake
//		mode - an integer with values ranging between 0 and 1
// Return: the shifted array
void shiftingArray(int array[], int cL, int mode)
{
	int i;
	if (mode==0)
	{
		// called in case of eating food
		for (i=cL+1;i>2/*SNAKE_MAX_LENGTH*/;i--)
			array[i] = array[i-1];
	}
	else
		if (mode==1)
		{
			for(i=cL+1;i>0/*SNAKE_MAX_LENGTH*/;i--)
				array[i] = array[i-1];
		}
}


// startSnakeGame()
// The most important function after main().
// Parameters:
// Return:
void startSnakeGame()
{
	int currentLength = 14; // initial snake length

	int historyX[SNAKE_MAX_LENGTH]; // for saving the x coordinates of the snake body
	int historyY[SNAKE_MAX_LENGTH]; // for saving the y coordinates of the snake body

	int x = 15;
	int y = 20;

	int foodPicked=0;
	int locFood[2];

	int collision=0;

	int direction[2];
	direction[0]=1;
	direction[1]=0;

	drawInit(historyX, historyY,x,y,currentLength);

	pickFoodLocation(36,100,locFood);
	printFoodItem(locFood);

	sleep(1);	

	while( ( verifyBounds(historyX[0],historyY[0]) ) && (collision==0) )
	{
		clrscr();
		printFoodItem(locFood);

		shiftingArray(historyX,currentLength,1);
		shiftingArray(historyY,currentLength,1);
		if (kbHit())
		{	
			// only if a predefined key is pressed the snake modifies it's direction
			directionalInput(direction);
		}
	
		updateHeadPosition(historyX,historyY,direction);
		drawSnake(historyX,historyY,currentLength);

		foodPicked=verifyFoodPick(historyX[0], historyY[0], locFood);
		if (foodPicked==1)
		{
			shiftingArray(historyX,currentLength,0);
			shiftingArray(historyY,currentLength,0);
			historyX[0]=historyX[1];
			historyY[0]=historyY[1];
			updateHeadPosition(historyX,historyY,direction);
			drawSnake(historyX,historyY,currentLength);
			currentLength++;

			pickFoodLocation(36,100,locFood);		

		}
	collision = collisionDetector(historyX, historyY,historyX[0],historyY[0], currentLength);

		sleep(1);
	
	}
}
 

// verifyBounds(int x, int y);
// Verifies if the (x,y) pair is into the bounds of the screen.
// Parameters: int x -> width of the canvas
// 		int y-> height of the canvas
// Return: 1 if the values are not in the range of 0->SCREEN_HEIGHT, 0->SCREEN_WIDTH
int verifyBounds(int x, int y)
{
	int boolean=0;
	if (!( (1>x) && (x<SCREEN_HEIGHT) ))
		if (!( (1>y) && (y<SCREEN_WIDTH)) )
			boolean=1;
	return boolean;
}


// verifyFoodPick(int x, int y, int *locFood)
//
// Parameters:  int x,y -> coordiantes of the head of the snake
//		int* locFood -> a poiter that points to an array that stores the x and y coordiantes
//				of the food location
// Return: 1 if it's a match, 0 else
int verifyFoodPick(int x, int y, int* locFood)
{
	int boolean=0;
	if ((x==*locFood) && (y==*(locFood+1)))
		boolean++;
	return boolean;
}


// updateHeadPosition(int hY[], int hX[], int* arrayP);
// Update the hX[0] and hY[0] variables with the values of pointer arrayP added with the values of
// hX[1] and hY[1].
// Parameters:  int hX[] - an array that stores the values of the x coordinate of the snake's body
//		int hY[] - an array that stores the values of the y coordinate of the snake's body
//		int *arrayP -> pointer to an array of 2 slots that stores the x and y coordinate of the translated key input
// Return: void
void updateHeadPosition(int hX[], int hY[], int *arrayP)
{
	hX[0]=hX[1] + *arrayP;
	hY[0]=hY[1] + *(arrayP+1); // *(arrayP+1) means the calue of the memory address given by the incemented position of the poiner arrayP

}
